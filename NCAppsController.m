#import <BulletinBoard/BBWeeAppController-Protocol.h>
#import <objc/runtime.h>

@interface SBBulletinListController
+(id)sharedInstanceIfExists;
-(void)hideListViewAnimated:(BOOL)animated;
@end

@interface SBIcon
-(UIImage*)getIconImage:(int)icon;
-(void)launchFromViewSwitcher;
@end

@interface SBIconModel
+(id)sharedInstance;
-(SBIcon*)leafIconForIdentifier:(NSString*)identifier;
@end

@interface NCAppsController : NSObject <BBWeeAppController> {
  SBIconModel* model;
  NSMutableArray* icons;
  UIView* view;
}
@end

@implementation NCAppsController
-(void)loadIcons {
  [icons removeAllObjects];
  for (NSString* identifier in [NSArray arrayWithContentsOfFile:
   @"/var/mobile/Library/Preferences/com.officialscheduler.ncapps.plist"]){
    [icons addObject:[model leafIconForIdentifier:identifier]];
  }
}
-(id)init {
  if((self=[super init])){
    model=[objc_getClass("SBIconModel") sharedInstance];
    icons=[[NSMutableArray alloc] init];
    [self loadIcons];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadIcons)
     name:@"SBIconModelDidReloadIconStateNotification" object:model];
  }
  return self;
}
-(void)handleTap:(UITapGestureRecognizer*)gesture {
  static Class cController=nil;
  if(!cController){cController=objc_getClass("SBBulletinListController");}
  UIView* iview=gesture.view;
  [UIView animateWithDuration:0.1
   animations:^{iview.transform=CGAffineTransformMakeScale(0.8,0.8);}
   completion:^(BOOL finished){
    [UIView animateWithDuration:0.15
     animations:^{iview.transform=CGAffineTransformIdentity;}
     completion:^(BOOL finished){
      [[cController sharedInstanceIfExists] hideListViewAnimated:YES];
      [[icons objectAtIndex:iview.tag] launchFromViewSwitcher];
    }];
  }];
}
-(UIView*)view {return view;}
-(float)viewHeight {return 75;}
-(void)loadPlaceholderView {
  view=[[UIView alloc] initWithFrame:CGRectMake(0,0,4,75)];
  UIView* bgview=[[UIView alloc] initWithFrame:CGRectMake(2,0,0,75)];
  view.autoresizingMask=bgview.autoresizingMask=UIViewAutoresizingFlexibleWidth;
  bgview.backgroundColor=[UIColor colorWithWhite:0 alpha:0.4];
  bgview.layer.cornerRadius=5;
  bgview.layer.masksToBounds=YES;
  NSUInteger count=icons.count,i;
  CGRect frame=CGRectMake(0,0,59,62);
  CGFloat W=(frame.size.width+3)*count-3;
  UIView* container=[[UIView alloc] initWithFrame:CGRectMake(-W/2,8,W,62)];
  container.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin
   |UIViewAutoresizingFlexibleRightMargin;
  for (i=0;i<count;i++){
    UIImageView* iview=[[UIImageView alloc] initWithFrame:frame];
    iview.tag=i;
    iview.image=[[icons objectAtIndex:i] getIconImage:2];
    iview.userInteractionEnabled=YES;
    UITapGestureRecognizer* gesture=[[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(handleTap:)];
    [iview addGestureRecognizer:gesture];
    [gesture release];
    [container addSubview:iview];
    [iview release];
    frame.origin.x+=frame.size.width+3;
  }
  [bgview addSubview:container];
  [view addSubview:bgview];
  [bgview release];
}
-(void)unloadView {
  [view release];
  view=nil;
}
-(void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [icons release];
  [view release];
  [super dealloc];
}
@end
