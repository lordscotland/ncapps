ARCHS = armv7

include theos/makefiles/common.mk

BUNDLE_NAME = NCApps
NCApps_FILES = NCAppsController.m
NCApps_FRAMEWORKS = UIKit CoreGraphics
NCApps_INSTALL_PATH = /Library/WeeLoader/Plugins

include $(THEOS_MAKE_PATH)/bundle.mk
